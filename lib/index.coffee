routes = (app) ->
  app.get '/', (req, res) ->
    res.render 'index.jade', title: 'Sample Title'

module.exports.routes = routes
