module.exports = (grunt) ->
  grunt.initConfig
    stylus:
      compile:
        options:
          compress: false
          linenos: true
          'include css': true
        files:
          'public/style.css': 'front/style/index.styl'

    browserify:
      compile:
        options:
          debug: true
          transform: ['coffeeify', 'debowerify']
          shim: grunt.file.readJSON 'front/shim.json'
        files:
          'public/script.js': 'front/script/index.coffee'

    bower:
      install:
        options:
          targetDir: 'front/lib/bower'

    watch:
      styles:
        files: ['front/style/**/*']
        tasks: ['stylus']
        options:
          spawn: true
      scripts:
        files: ['front/script/**/*']
        tasks: ['browserify']
        options:
          spawn: true

    uglify:
      compile:
        files:
          'public/script.min.js': 'public/script.js'

    cssmin:
      compile:
        files:
          'public/style.min.css': 'public/style.css'

    rename:
      compile:
        files:
          'public/style.css': 'public/style.min.css'
          'public/script.js': 'public/script.min.js'

    nodemon:
      dev:
        options:
          file: 'index.coffee'
          nodeArgs: []
          ignoredFiles: ['node_modules/**', 'front/**', 'public/**']
          watchedExtensions: ['js', 'coffee']
          delayTime: 1
          env:
            PORT: 3000

  grunt.loadNpmTasks 'grunt-browserify'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-stylus'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-cssmin'
  grunt.loadNpmTasks 'grunt-rename'
  grunt.loadNpmTasks 'grunt-bower-task'
  grunt.loadNpmTasks 'grunt-nodemon'

  grunt.registerTask 'default', ['bower', 'browserify', 'stylus']
  grunt.registerTask 'dev', ['bower', 'browserify', 'stylus']
  grunt.registerTask 'dist', ['dev', 'uglify', 'cssmin', 'rename']
