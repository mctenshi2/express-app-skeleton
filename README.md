Express Skeleton With Frontend Resources
========================================

This repository is an node based express.js skeleton app which includes several frontend features.

  * [express.js 3.x](http://expressjs.com)
  * [grunt](http://gruntjs.com/)
  * [bower](http://bower.io/)
  * [browserify](http://browserify.org/) (via coffeescript -> [coffeeify](https://github.com/jnordberg/coffeeify)
  * [coffeescript](http://coffeescript.org/) (node + frontend)
  * [jade](http://jade-lang.com/)
  * [stylus](http://learnboost.github.io/stylus/)

Frontend Development
-------------------

There are some features to help frontend development process.

  * `grunt browserify`: grunt command that build coffeescript codes with sourcemap which helps debug.
  * `grunt stylus`: grunt command build stylus code
  * `grunt dist`: grunt command for production (build and minify)
  * `grunt watch`: grunt command for automatic build coffeescript and stylus

Guide to use this app skeleton
------------------------------

  1. git clone this repo
  2. install grunt-cli if you didn't install one: `$ npm install -g grunt-cli`
  3. `$ npm install`
  4. `$ grunt bower:install`
  5. `$ grunt watch` to watch your frontend files changing.
  6. `$ grunt nodemon` to run your server!
